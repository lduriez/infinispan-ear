package org.lduriez.jcache.model;

/**
 * Non serializable class 
 *
 */
public class NonSerializable {

	private String s;

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}
	
}
