package org.lduriez.jcache.model;

import java.io.Serializable;

/**
 * DTO representing a city
 *
 */
public class CityDto implements Serializable {
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * City name
	 */
	private String name;

	/* Uncomment to trigger an exception during java/jboss serialization */
	/*
	 * private NonSerializable ns;
	 * 
	 * public NonSerializable getNs() { return ns; }
	 */

	/**
	 * Constructor
	 * 
	 * @param name City name
	 */
	public CityDto(String name) {
		super();
		this.name = name;
		/* Uncomment to trigger an exception during java/jboss serialization */
		// ns = new NonSerializable();
		// ns.setS("ns");
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
