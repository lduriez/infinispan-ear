package org.lduriez.jcache.infinispan.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.cache.annotation.CacheRemoveAll;
import javax.cache.annotation.CacheResult;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.infinispan.Cache;
import org.infinispan.CacheSet;
import org.infinispan.configuration.cache.Configuration;
import org.lduriez.jcache.infinispan.config.CityCache;
import org.lduriez.jcache.model.CityDto;

/**
 * Business service for City
 *
 */
@ApplicationScoped
public class CityService {

	/**
	 * Infinispan cache city-cache
	 */
	@Inject
	@CityCache
	private Cache<CacheSet<Object>, Collection<Object>> cache;

	/**
	 * Fetch cities from datastore
	 * 
	 * @return all cities, unsorted
	 */
	@CacheResult(cacheName = "city-cache")
	public List<CityDto> getCities() {
		System.out.println("Fetching cities...");
		List<CityDto> cities = new ArrayList<>();
		cities.add(new CityDto("Paris"));
		cities.add(new CityDto("Suresnes"));
		return cities;
	}

	/**
	 * Clear city cache
	 * 
	 */
	@CacheRemoveAll(cacheName = "city-cache")
	public void clearCityCache() {
		System.out.println("-- Clearing city cache --");
	}

	/**
	 * Get city cache info
	 * 
	 * @return city cache info as Sting
	 */
	public String getCacheInfo() {
		Configuration configuration = this.cache.getCacheConfiguration();
		String cacheMode = configuration.clustering().cacheMode().toString();
		long size = configuration.memory().size();
		long timeout = configuration.clustering().stateTransfer().timeout();
		long remoteTimeout = configuration.clustering().remoteTimeout();
		StringBuilder result = new StringBuilder();
		result.append(" cacheMode = " + cacheMode);
		result.append(" Size = " + size);
		result.append(" timeout = " + timeout);
		result.append(" remoteTimeout = " + remoteTimeout);
		result.append(" entries = " + this.cache.size());
		return result.toString();
	}

}
