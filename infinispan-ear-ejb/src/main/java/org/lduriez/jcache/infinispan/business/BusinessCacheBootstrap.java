package org.lduriez.jcache.infinispan.business;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Bootstrap business cache at start-up
 *
 */
@Startup
@Singleton
public class BusinessCacheBootstrap {
	
	@Inject
	private CityService cityService;
	
	/**
	 * Log startup information
	 * 
	 */
	@PostConstruct
	public void bootstrapBusinessCache() {
		System.out.println("BEGIN bootstrap infinispan cache manager and caches");
		this.cityService.getCities();
		System.out.println("END bootstrap infinispan cache manager and caches");
	}
}
