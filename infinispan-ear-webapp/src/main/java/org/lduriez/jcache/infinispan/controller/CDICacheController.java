package org.lduriez.jcache.infinispan.controller;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.lduriez.jcache.infinispan.business.CityService;
import org.lduriez.jcache.infinispan.business.CountryService;
import org.lduriez.jcache.model.CityDto;
import org.lduriez.jcache.model.CountryDto;

/**
 * Controller for CDI cache testing
 *
 */
@Named("CDICacheController")
@RequestScoped
public class CDICacheController {

	@Inject
	private CityService cityService;
	
	@Inject
	private CountryService countryService;
	
	/**
	 * Clear city cache
	 * 
	 * @return "OK"
	 */
	public String getClearCityCache() {
		cityService.clearCityCache();
		return "OK";
	}
	
	/**
	 * Fetch cities from datastore
	 * 
	 * @return all cities, unsorted
	 */
	public List<CityDto> getCities() {
		return cityService.getCities();
	}
	
	/**
	 * Fetch countries from datastore
	 * 
	 * @return all countries, unsorted
	 */
	public List<CountryDto> getCountries() {
		return countryService.getCountries();
	}
	
	public String getCityCacheInfo() {
		return cityService.getCacheInfo();
	}
}
