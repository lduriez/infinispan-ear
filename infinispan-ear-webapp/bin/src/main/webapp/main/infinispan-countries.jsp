<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Countries</title>
</head>
<body>
	<table>
		<c:forEach items="${CDICacheController.countries}" var="country">
			<tr>
				<td>${country.name}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>