package org.lduriez.jcache.infinispan.listener;


import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryCreated;
import org.infinispan.notifications.cachelistener.event.CacheEntryCreatedEvent;

/**
 * Clustered listener for city cache events
 *
 */
@Listener(clustered = true)
public class CityCacheListener {
	@CacheEntryCreated
	public void listen(CacheEntryCreatedEvent event) {
		System.out.println("-- City cache entry added --");
	}
}
