package org.lduriez.jcache.infinispan.config;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;

import org.infinispan.Cache;
import org.infinispan.CacheSet;
import org.infinispan.commons.api.CacheContainerAdmin;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.jboss.marshalling.core.JBossUserMarshaller;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.transaction.TransactionMode;
import org.lduriez.jcache.infinispan.listener.CityCacheListener;

/**
 * Handles application cache setup
 */
@ApplicationScoped
public class InfinispanCacheConfig {

	/**
	 * City cache name
	 */
	public static final String CITY_CACHE = "city-cache";
	
	/**
	 * Country cache name
	 */
	public static final String COUNTRY_CACHE = "country-cache";

	/**
	 * Application cache manager
	 */
	private EmbeddedCacheManager embeddedCacheManager;

	/**
	 * Application global cache configuration
	 */
	private GlobalConfiguration globalConfiguration;

	/**
	 * Creates {@link org.infinispan.manager.EmbeddedCacheManager} with JMX enabled.
	 *
	 * @return EmbeddedCacheManager Clustered cache manager
	 * @throws UnknownHostException
	 */
	@Produces
	@ApplicationScoped
	public EmbeddedCacheManager defaultClusteredCacheManager() throws UnknownHostException {
		System.out.println("-- Initialize infinispan cache manager --");
		Configuration defaultConfiguration = getDefaultConfiguration();
		this.globalConfiguration = getGlobalConfiguration();
		this.embeddedCacheManager = new DefaultCacheManager(this.globalConfiguration, defaultConfiguration);
		this.embeddedCacheManager.administration().withFlags(CacheContainerAdmin.AdminFlag.VOLATILE)
				.getOrCreateCache(CITY_CACHE, getCityCacheConfiguration()).addListener(new CityCacheListener());
		this.embeddedCacheManager.administration().withFlags(CacheContainerAdmin.AdminFlag.VOLATILE)
				.getOrCreateCache(COUNTRY_CACHE, getCountryCacheConfiguration());
		return this.embeddedCacheManager;
	}

	/**
	 * Set global configuration shared by all caches
	 * 
	 * @return Cache global configuration
	 * @throws UnknownHostException
	 */
	private GlobalConfiguration getGlobalConfiguration() throws UnknownHostException {
		GlobalConfigurationBuilder globalConfigurationBuilder = GlobalConfigurationBuilder.defaultClusteredBuilder();
		globalConfigurationBuilder.cacheManagerName("test-infinispan-cache-manager");
		globalConfigurationBuilder.defaultCacheName("test-infinispan-default-cache");
		globalConfigurationBuilder.jmx().enable().domain("test-infinispan-jmx-domain");
		globalConfigurationBuilder.transport().defaultTransport().clusterName("test-infinispan-local-cluster");
		globalConfigurationBuilder.transport().initialClusterTimeout(30, TimeUnit.SECONDS);
		globalConfigurationBuilder.serialization().marshaller(new JBossUserMarshaller()).whiteList()
				.addRegexps("org.lduriez.jcache.infinispan.*");
		return globalConfigurationBuilder.build();
	}

	@CityCache
	@Produces
	@ApplicationScoped
	public Cache<CacheSet<Object>, Collection<Object>> getCityCache() {
		return this.embeddedCacheManager.getCache(CITY_CACHE);
	}

	/**
	 * Configure city cache
	 * 
	 * @return City cache configuration
	 */
	private Configuration getCityCacheConfiguration() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.clustering().cacheMode(CacheMode.REPL_SYNC);
		configurationBuilder.clustering().stateTransfer().fetchInMemoryState(true);
		configurationBuilder.clustering().stateTransfer().awaitInitialTransfer(true);
		configurationBuilder.clustering().stateTransfer().timeout(30, TimeUnit.SECONDS);
		configurationBuilder.clustering().remoteTimeout(30, TimeUnit.SECONDS);
		configurationBuilder.transaction().transactionMode(TransactionMode.NON_TRANSACTIONAL);
		configurationBuilder.memory().size(10000);
		return configurationBuilder.build();
	}

	@CountryCache
	@Produces
	@ApplicationScoped
	public Cache<CacheSet<Object>, Collection<Object>> getCountryCache() {
		return this.embeddedCacheManager.getCache(COUNTRY_CACHE);
	}
	
	/**
	 * Configure country cache
	 * 
	 * @return Country cache configuration
	 */
	private Configuration getCountryCacheConfiguration() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.clustering().cacheMode(CacheMode.REPL_SYNC);
		configurationBuilder.clustering().stateTransfer().fetchInMemoryState(true);
		configurationBuilder.clustering().stateTransfer().awaitInitialTransfer(true);
		configurationBuilder.clustering().stateTransfer().timeout(30, TimeUnit.SECONDS);
		configurationBuilder.clustering().remoteTimeout(30, TimeUnit.SECONDS);
		configurationBuilder.transaction().transactionMode(TransactionMode.NON_TRANSACTIONAL);
		configurationBuilder.memory().size(100);
		return configurationBuilder.build();
	}

	/**
	 * Configure default cache configuration
	 * 
	 * @return Default cache configuration
	 */
	private Configuration getDefaultConfiguration() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.clustering().cacheMode(CacheMode.REPL_SYNC);
		configurationBuilder.transaction().transactionMode(TransactionMode.NON_TRANSACTIONAL);
		configurationBuilder.clustering().stateTransfer().timeout(30, TimeUnit.SECONDS);
		configurationBuilder.clustering().remoteTimeout(30, TimeUnit.SECONDS);
		configurationBuilder.memory().size(1000);
		return configurationBuilder.build();
	}

	/**
	 * CDI event initialized
	 * 
	 * @param init
	 */
	public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
		System.out.println("-- CDI Initialized --");
	}

	/**
	 * CDI event destroyed
	 * 
	 * @param init
	 * @throws IOException
	 */
	public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init) throws IOException {
		System.out.println("Closing infinispan cache manager has been closed");
		this.embeddedCacheManager.close();
		System.out.println("Infinispan cache manager has been closed");
	}

}
