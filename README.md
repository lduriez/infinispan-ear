infinispan-webapp test : Test Infinispan with JCache annotations, EAR app
=====================================
Author: Lionel Duriez

Tested on
--------------------
JBoss EAP 7.2 with Infinispan Wildfly modules 10.1.7.Final

Install Infinispan Wildfly Modules
--------------------

Download from https://infinispan.org/download/

Copy modules directory found in the archive to $JBOSS_HOME directory. 

Deploy application on two JBoss servers
---------------------

Server 1 : localhost:8080
Server 2 : localhost:8180

Access the application
---------------------

Open http://localhost:8080/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan-cities.jsp => Cities are displayed

Open http://localhost:8180/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan-cities.jsp => Cities are displayed

Open http://localhost:8180/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan-modify-cities.jsp => City cache is cleared

Open http://localhost:8080/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan-cities.jsp => Cities are displayed
Cities are fetched from datastore and displayed

Open http://localhost:8180/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan-cities.jsp => Cities are displayed with no datastore access

TODO
--------------------

1. Improve cache configuration
2. Choose serialization solution
