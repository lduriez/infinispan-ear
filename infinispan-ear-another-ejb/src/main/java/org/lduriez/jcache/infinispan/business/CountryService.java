package org.lduriez.jcache.infinispan.business;

import java.util.ArrayList;
import java.util.List;

import javax.cache.annotation.CacheResult;
import javax.enterprise.context.ApplicationScoped;

import org.lduriez.jcache.model.CountryDto;

/**
 * Business service for Country
 *
 */
@ApplicationScoped
public class CountryService {
	
	/**
	 * Fetch countries from datastore
	 * 
	 * @return all countries, unsorted
	 */
	@CacheResult(cacheName = "country-cache")
	public List<CountryDto> getCountries() {
		System.out.println("Fetching countries...");
		List<CountryDto> countries = new ArrayList<>();
		countries.add(new CountryDto("France"));
		countries.add(new CountryDto("Espagne"));
		return countries;
	}
}
