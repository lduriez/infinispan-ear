package org.lduriez.jcache.model;

import java.io.Serializable;

/**
 * DTO representing a country
 *
 */
public class CountryDto implements Serializable {
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Country name
	 */
	private String name;

	
	/**
	 * Constructor
	 * @param name Country name
	 */
	public CountryDto(String name) {
		super();
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
